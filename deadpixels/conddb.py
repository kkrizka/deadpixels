"""
Manipulation of the pixel dead module map stored in the conditions database.
"""

import pandas as pd
import numpy as np

def deadmap(run, db, conditionsTag, lb=0):
    """
    Return the dead pixel map for a given run and LB as a pd.DataFrame.
    """
    # Query the database to get the PixelModuleFeMask object
    PixelModuleFeMask=db.getFolder('/PIXEL/PixelModuleFeMask')

    myiov=(run<<32)+lb
    obj=PixelModuleFeMask.findObject(myiov,0,conditionsTag)

    PixelModuleFeMask=eval(obj.payload()[0])

    # Convert to a DataFrame with helpful id stuff
    data={'moduleID':[],'status':[]}
    for k,v in PixelModuleFeMask.items():
        data['moduleID'].append(int(k))
        data['status'].append(v)
    PixelModuleFeMask=pd.DataFrame(data)

    return PixelModuleFeMask
