import numpy as np

from matplotlib.patches import Rectangle

def vmax_occupancy(values):
    """
    Calculate the recommended `vmax` range that clips the out outliers from
    an occupancy plot.
    """
    return np.median(values)*3

def occupancy(hist, ax):
    """
    Plot a pixel occupancy plot from a DQM histogram.
    """

    values=hist.values().T
    ax.imshow(values, vmax=vmax_occupancy(values), aspect='auto', interpolation='none', origin='lower')

def deadfe_endcap(deadmap, ax):
    """
    Highlight the dead modules / FEs using a dead module map.
    """
    for idx, deadmodule in deadmap.iterrows():
        if deadmodule.status==0: # The whole module is dead
            ax.add_patch(Rectangle((deadmodule.Layer_Disk*8-0.5, deadmodule.Phi_module*2-0.5),8,2, edgecolor='red', fill=False, lw=2))
        else:
            for i in range(16):
                """
                7 6 ...  1  0
                8 9 ... 14 15
                """
                if deadmodule.status & (1<<i):
                    x=i%8
                    y=(15-i)//8
                    if y==1: # upper row is backwards
                        x=7-x
                    ax.add_patch(Rectangle((deadmodule.Layer_Disk*8+x-0.5, deadmodule.Phi_module*2+y-0.5),1,1, edgecolor='red', fill=False, lw=2))

def deadfe_barrel(deadmap, ax):
    """
    Highlight the dead modules / FEs using a dead module map.

    The overlay is based on the dead module Eta_module and Phi_module indexes.
    The Eta_module is shifted by +6 to acccount for the range of the Eta_module
    index being from -6 to 6.
    """
    for idx, deadmodule in deadmap.iterrows():
        if deadmodule.status==0: # The whole module is dead
            ax.add_patch(Rectangle(((6+deadmodule.Eta_module)*8-0.5, deadmodule.Phi_module*2-0.5),8,2, edgecolor='red', fill=False, lw=2))
        else:
            for i in range(16):
                """
                7 6 ...  1  0
                8 9 ... 14 15
                """
                if deadmodule.status & (1<<i):
                    x=i%8
                    y=(15-i)//8
                    if y==1: # upper row is backwards
                        x=7-x
                    ax.add_patch(Rectangle(((6+deadmodule.Eta_module)*8+x-0.5, deadmodule.Phi_module*2+y-0.5),1,1, edgecolor='red', fill=False, lw=2))

def axis_endcap(moduleidmap,ax):
    """
    Prepare the x/y axes for an endcap plot. This is done independently of any
    drawing to ensure that the axes are always the same.

    The axes are defined within the `moduleidmap` DataFrame (see
    `idmap.PixelIDMap`).
    """
    nFEperRow=8
    nFEperCol=2

    # Get the x/y counts

    ringidmap = moduleidmap[['Layer_Disk','geographicalID']].copy()
    ringidmap['ringName'] = ringidmap.geographicalID.str.extract(r'(D[0-9][A-Z])')
    ringNames=ringidmap.groupby('Layer_Disk').ringName.unique().str[0]

    phiidmap = moduleidmap[['Phi_module','geographicalID']].copy()
    phiidmap['phiName'] = phiidmap.geographicalID.str.extract(r'D[0-9][A-Z]_([A-Z][0-9]+_[A-Z][0-9]+_[A-Z][0-9]+)')
    phiNames=phiidmap.groupby('Phi_module').phiName.unique().str[0]

    #
    # Handle the x axis
    ax.set_xticks(nFEperRow/2+np.arange(0, ringNames.size*nFEperRow, nFEperRow)-0.5)
    ax.set_xticklabels(ringNames.values)

    for disk in range(ringNames.size):
        ax.axvline(disk*nFEperRow-0.5,0 ,phiNames.size*nFEperCol, color='gray', linewidth=0.5)

    ax.set_xlim(-0.5, ringNames.size*nFEperRow-0.5)

    #
    # Handle the y axis
    ax.set_yticks(nFEperCol/2+np.arange(0, phiNames.size*nFEperCol, nFEperCol)-0.5)
    ax.set_yticklabels(phiNames.values)

    for phi in range(phiNames.size):
        ax.axhline(phi*nFEperCol-0.5,0 ,ringNames.size*nFEperRow, color='gray', linewidth=0.5)

    ax.set_ylim(-0.5, phiNames.size*nFEperCol-0.5)

def axis_barrel(moduleidmap,ax):
    """
    Prepare the x/y axes for an endcap plot. This is done independently of any
    drawing to ensure that the axes are always the same.

    The axes are defined within the `moduleidmap` DataFrame (see
    `idmap.PixelIDMap`).
    """
    nFEperRow=8
    nFEperCol=2

    # Get the x/y counts

    etaidmap = moduleidmap[['Eta_module','geographicalID']].copy()
    etaidmap['etaName'] = etaidmap.geographicalID.str.extract(r'L[A-Z0-9]_B[0-9]+_S[0-9]+_[A-Z][0-9]+_(M[A-Z0-9]+)')
    etaidmap=etaidmap.groupby('Eta_module').etaName.unique().str[0]

    phiidmap = moduleidmap[['Phi_module','geographicalID']].copy()
    phiidmap['phiName'] = phiidmap.geographicalID.str.extract(r'L[A-Z0-9]_(B[0-9]+_S[0-9]+)_[A-Z][0-9]+_M[A-Z0-9]+')
    phiNames=phiidmap.groupby('Phi_module').phiName.unique().str[0]

    #
    # Handle the x axis
    ax.set_xticks(nFEperRow/2+np.arange(0, etaidmap.size*nFEperRow, nFEperRow)-0.5)
    ax.set_xticklabels(etaidmap.values)

    for disk in range(etaidmap.size):
        ax.axvline(disk*nFEperRow-0.5,0 ,phiNames.size*nFEperCol, color='gray', linewidth=0.5)

    ax.set_xlim(-0.5, etaidmap.size*nFEperRow-0.5)

    #
    # Handle the y axis
    ax.set_yticks(nFEperCol/2+np.arange(0, phiNames.size*nFEperCol, nFEperCol)-0.5)
    ax.set_yticklabels(phiNames.values)

    for phi in range(phiNames.size):
        ax.axhline(phi*nFEperCol-0.5,0 ,etaidmap.size*nFEperRow, color='gray', linewidth=0.5)

    ax.set_ylim(-0.5, phiNames.size*nFEperCol-0.5)


def axis_ibl(moduleidmap,ax):
    """
    Prepare the x/y axes for an endcap plot. This is done independently of any
    drawing to ensure that the axes are always the same.

    The axes are defined within the `moduleidmap` DataFrame (see
    `idmap.PixelIDMap`).
    """
    # Get the x/y counts

    nFEperCol=1

    etaidmap = moduleidmap[['Eta_module','geographicalID']].copy()
    etaidmap['etaName'] = etaidmap.geographicalID.str.extract(r'LI_S[0-9]+_[AC]_M[0-9]+_([AC][0-9]+_?[0-9]?)')
    etaidmap=etaidmap.groupby('Eta_module').etaName.unique().str[0]
    nFE=etaidmap.str.contains('_').map({True:1,False:2})
    sft=etaidmap.str.contains('_').map({True:1,False:1.5})

    phiidmap = moduleidmap[['Phi_module','geographicalID']].copy()
    phiidmap['phiName'] = phiidmap.geographicalID.str.extract(r'LI_(S[0-9]+)_[AC]_M[0-9]+_[AC][0-9]+_?[0-9]?')
    phiNames=phiidmap.groupby('Phi_module').phiName.unique().str[0]

    #
    # Handle the x axis
    ax.set_xticks(nFE.cumsum()-sft)
    ax.set_xticklabels(etaidmap.values)

    for disk in nFE.cumsum():
        ax.axvline(disk-0.5,0 ,phiNames.size*nFEperCol, color='gray', linewidth=0.5)

    xmax=nFE.sum()-0.5
    ax.set_xlim(-0.5, xmax)

    #
    # Handle the y axis
    ax.set_yticks(nFEperCol/2+np.arange(0, phiNames.size*nFEperCol, nFEperCol)-0.5)
    ax.set_yticklabels(phiNames.values)

    for phi in range(phiNames.size):
        ax.axhline(phi*nFEperCol-0.5,0 , xmax, color='gray', linewidth=0.5)

    ax.set_ylim(-0.5, phiNames.size*nFEperCol-0.5)
