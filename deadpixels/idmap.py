import pandas as pd

class PixelIDMap:
    """
    Class to provide a conversion between the different ID schamas used by the
    pixel detector.
    """
    def __init__(self, mapfile):
        self.moduleidmap=pd.read_csv(mapfile,sep='\t',index_col=False)
        self.moduleidmap=self.moduleidmap.set_index('hashID')

    def map(self, df):
        """
        Decorate the contents of dataframe `df` with alternate module IDs using the
        map stored here. The `df` is expected to have a column called `moduleID`
        that maps to the index of the module id map.
        """
        return df.merge(self.moduleidmap,left_on='moduleID',right_index=True)
