#!/usr/bin/env python
"""
Create a dead module map based on occupancy histograms from DQM output. The map
is created at module level. Individual FEs cannot be currently addressed due to
unknown FE ID maps.

If specified, an existing conditions tag is retrieved and merged with the new
result.

The IBL is currently ignored.
"""

# %%
import re
import sys
import json
import uproot
import pathlib
import importlib_resources

import pandas as pd
import numpy as np

from deadpixels import plot, idmap, conddb

from PyCool import cool,coral

# %% inputs
if 'ipykernel' in sys.modules: # running in a notebook
    #%load_ext autoreload
    #%autoreload 2
    dqm='data22_13p6TeV.00427929.physics_MinBias.recon.HIST.f1250._lb0257._0005.1'
    tag='PixelModuleFeMask-Run3-MinBias-01'
    conditionsTag = None
    dbstring = None
    outdb = 'sqlite://;schema=mycool.db;dbname=OFLP200'
else:
    import argparse
    parser = argparse.ArgumentParser(description='Plot dead module map')
    parser.add_argument('dqm', help='DQM histograms with module occupancy.')
    parser.add_argument('tag', help='New tag name for the conditions database output.')
    parser.add_argument('--conditionsTag', help='Conditions tag to use for /PIXEL/PixelModuleFeMask.', default=None)
    parser.add_argument('--db', help='Database string.', default='COOLOFL_PIXEL/CONDBR2')
    parser.add_argument('--outdb', help='Output database specification.', default='sqlite://;schema=mycool.db;dbname=OFLP200')
    args = parser.parse_args()

    dqm = args.dqm
    tag = args.tag
    conditionsTag = args.conditionsTag
    dbstring = args.db
    outdb = args.outdb

# %% Create the database connection
dbSvc=cool.DatabaseSvcFactory.databaseService()
try:
    db=dbSvc.openDatabase(dbstring)
except Exception as e:
    print('Problem opening database',e)
    sys.exit(-1)

# %% Prepare the module map
moduleidmap=idmap.PixelIDMap(importlib_resources.files('deadpixels.data').joinpath('PixelMapping.csv'))

# Remove the weird ID insertion thing

geoID=moduleidmap.moduleidmap['geographicalID']
for toremove in ['C6','C7','A6','A7']:
    geoID=geoID.str.replace(f'_{toremove}_','_')
moduleidmap.moduleidmap['geoID']=geoID

# %% Load the histograms
re_occhist=re.compile('run_([0-9]*)/Pixel/Clusters/ClusterFEOccupancy_([A-Za-z0-9]+)')
fh=uproot.open(dqm)
hists=list(filter(lambda key: re_occhist.match(key) is not None, fh.keys()))

# %%
re_disklabel=re.compile(r'Disk([0-9]+)_FE_([0-9]+)_([0-9]+)')
re_philabel=re.compile(r'([A-Z0-9_]+)_FE_([0-9]+)_([0-9]+)')
re_etalabel=re.compile(r'([A-Z0-9_]+)_FE#([0-9]+)')

runs=set() # List of runs to merge with
deads=pd.DataFrame(columns=['module','FE'])
for hname in hists:
    print(f'Checking {hname}')

    # Extract histogram and information about it
    info=re_occhist.match(hname)
    run=int(info.group(1))
    sub=info.group(2)
    hist=fh[hname]
    #if sub!='IBL': continue

    runs.add(run)

    if sub.startswith('EC'):
        re_x=re_disklabel
        re_y=re_etalabel
        geostr='D{x}{Layer_Disk}_{y}'
        Layer_Disk=sub[-1]
    elif 'Layer' in sub:
        re_x=re_philabel
        re_y=re_etalabel
        geostr='L{Layer_Disk}_{y}_{x}'
        Layer_Disk={'BLayer':0,'Layer1':1,'Layer2':2}[sub]
    else:
        print(f'Unknown subdetector {sub}')
        continue

    values=hist.values()

    xlabels=hist.axes[0].labels()
    ylabels=hist.axes[1].labels()

    for i,j in zip(*np.where(values==0)):
        x=re_x.match(xlabels[i])
        y=re_y.match(ylabels[j])

        # calculate the front end number
        row=int(y.group(2))
        FE=int(x.group(1+row))

        # calculate the geographical identifier
        geoID=geostr.format(Layer_Disk=Layer_Disk,x=x.group(1),y=y.group(1))
        #print(geoID)

        # Add the module hash/frontend
        mymodule=moduleidmap.moduleidmap[moduleidmap.moduleidmap.geoID==geoID]
        deads=pd.concat([deads,pd.DataFrame({'module':mymodule.index ,'FE':FE})])

# Merge the dead FEs into a status map
deads=deads.groupby('module').agg(lambda x: np.left_shift(1,x).sum())

# For now, only deal with fully dead modules
deads=deads[deads.FE==0xFFFF]

# Merge with an existing conditions tag, if it exists
if conditionsTag is not None:
    for run in runs:
        knowndeads=conddb.deadmap(run, db, conditionsTag, lb=257)
        mask_deadmod=knowndeads.status==0

        knowndeads.loc[mask_deadmod,'status']=0xFFFF
        knowndeads=knowndeads.set_index('moduleID')

        mydeads=deads.merge(knowndeads,how='outer',left_index=True,right_index=True,indicator=True)

        mydeads.loc[mydeads._merge=='left_only' ,'status']=0
        mydeads.loc[mydeads._merge=='right_only','FE'    ]=0

        mydeads['FE'    ]=mydeads.FE.astype(int)
        mydeads['status']=mydeads.status.astype(int)

        mydeads['FE']=np.bitwise_or(mydeads['FE'],mydeads['status'])

        deads=mydeads.drop(['_merge','status'],axis=1)

# For now, only deal with fully dead modules
deads.index=deads.index.astype(str)
deads.loc[deads.FE==0xFFFF,'FE']=0 # zero means completely dead module

# Write the data to a local database
path   = '/PIXEL/PixelModuleFeMask'
since  = 410000<<32
until  = 0x7fffffffffffffff

try:
    db = dbSvc.createDatabase(outdb)
except Exception as e:
    print('Problem opening database',e.what())
    sys.exit(-1)

spec = cool.RecordSpecification()
spec.extend('data_array', cool.StorageType.String16M)
desc = '<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName>'
folderSpec = cool.FolderSpecification(cool.FolderVersioning.MULTI_VERSION, spec)
folder = db.createFolder(path, folderSpec, desc, True)
data = cool.Record(spec)

data['data_array'] = json.dumps(deads['FE'].to_dict())
folder.storeObject(since,until,data,0,tag)

db.closeDatabase()
