#!/usr/bin/env python
"""
Plots the dead module map retrieved from the database. Optinally, it can draw
on top of an occupancy map from the DQM output.
"""

# %%
import re
import sys
import uproot
import importlib_resources

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

from deadpixels import plot, idmap, conddb

from PyCool import cool,coral

# %% inputs
if 'ipykernel' in sys.modules: # running in a notebook
    #%load_ext autoreload
    #%autoreload 2
    dqm='data22_13p6TeV.00427929.physics_MinBias.recon.HIST.f1250._lb0257._0005.1'
    conditionsTag = None
    dbstring = None
else:
    import argparse
    parser = argparse.ArgumentParser(description='Plot dead module map')
    parser.add_argument('--dqm', help='DQM histograms with module occupancy.', default=None)
    parser.add_argument('--conditionsTag', help='Conditions tag to use for /PIXEL/PixelModuleFeMask.', default=None)
    parser.add_argument('--db', help='Database string.', default='COOLOFL_PIXEL/CONDBR2')
    args = parser.parse_args()

    dqm = args.dqm
    conditionsTag = args.conditionsTag
    dbstring = args.db

# %% Create the database connection
dbSvc=cool.DatabaseSvcFactory.databaseService()
try:
    db=dbSvc.openDatabase(dbstring)
except Exception as e:
    print('Problem opening database',e)
    sys.exit(-1)

# %% Prepare the module map
moduleidmap=idmap.PixelIDMap(importlib_resources.files('deadpixels.data').joinpath('PixelMapping.csv'))

# %% Load the histograms
re_occhist=re.compile('run_([0-9]*)/lb_([0-9]*)/Pixel/LumiBlock/ClusterFEOccupancyLB_([A-Za-z0-9]+)')
if dqm is not None:
    fh=uproot.open(dqm)
    hists=list(filter(lambda key: re_occhist.match(key) is not None, fh.keys()))
else:
    fh=None
    hists=[]

# %%
for hname in hists:
    print(f'Plotting {hname}')

    # Extract histogram and information about it
    info=re_occhist.match(hname)
    run=int(info.group(1))
    lb=int(info.group(2))
    sub=info.group(3)
    if fh is not None:
        hist=fh[hname]
    else:
        hist=None

    # Get the endcap vs barrel
    Barrel_EC=0
    if sub.startswith('EC'):
        type='endcap'
        Barrel_EC=2 if sub=='ECA' else -2
        Layer_Disk=None
        faxis=plot.axis_endcap
        fmap=plot.deadfe_endcap
    elif 'Layer' in sub:
        type='barrel'
        Barrel_EC=0
        Layer_Disk={'BLayer':1,'Layer1':2,'Layer2':3}.get(sub,1)
        faxis=plot.axis_barrel
        fmap=plot.deadfe_barrel
    elif sub=='IBL':
        type='barrel'
        Barrel_EC=0
        Layer_Disk=0
        faxis=plot.axis_ibl
        fmap=plot.deadfe_barrel
    else:
        print(f'Unknown subdetector {sub}')
        continue

    if conditionsTag is not None:
        deadmap=conddb.deadmap(run, db, conditionsTag, lb=lb)
        deadmap=moduleidmap.map(deadmap)
        mask=(deadmap.Barrel_EC==Barrel_EC) & ((deadmap.Layer_Disk==Layer_Disk)|(Layer_Disk is None))
        mydeadmap=deadmap[mask]
    else:
        deadmap=None
        mydeadmap=None

    fig, ax = plt.subplots(figsize=(10,15))

    mask=(moduleidmap.moduleidmap.Barrel_EC==Barrel_EC) & ((moduleidmap.moduleidmap.Layer_Disk==Layer_Disk)|(Layer_Disk is None))
    mymoduleidmap=moduleidmap.moduleidmap[mask]
    faxis(mymoduleidmap, ax)

    if hist is not None:
        plot.occupancy(hist, ax=ax)

    if mydeadmap is not None:
        fmap(mydeadmap, ax=ax)

    title=[f'run {run}', f'lb {lb}', sub]
    if conditionsTag is not None:
        title.append(conditionsTag)
    ax.set_title(', '.join(title))

    fig.tight_layout()
    fig.savefig(f'run{run}_lb{lb}_{sub}.png')
    fig.show()

# %%
