A collection of scripts for visualizing and generating the pixel detector dead module map.

# Installation
Due to usage of the conditions database (via PyCool), this needs to be setup
inside an Athena release.

```bash
asetup Athena,22.0.82
python -m venv .venv --system-site-packages
source .venv/bin/activate
pip install .
```

The environment can be reactivated in a new session with the following commands.
```bash
asetup --restore
source .venv/bin/activate
```

# Usage
Two operations are currently supported; plotting an existing occupancy map with
an overlay of the dead modules, and generating a new map from a given run. The
two commands are `plot_deadmodulemap.py` and `create_deadmodulemap.py`. Details
on functionality and options are included in each script

The TL;DR instructions are as follows.

A plot for a DQM histogram can be created with the following. Note that both the
DQM histogram path and conditions tag are optional. Ignoring either will
disable the drawing of the occupancy histogram and dead module overlay
respectively.

```bash
plot_deadmodulemap.py --dqm data22_13p6TeV.00427929.physics_Main.recon.HIST.f1250._lb0100._0003.1  --conditionsTag PixelModuleFeMask-RUN2-DATA-UPD4-09
```

A dead module map can be created with the following. The output is saved to
a file called `mycool.db`. Alternate output can be specified via the `--outdb`
option. Note that the output is always created and fails if it already exists.
```bash
create_deadmodulemap.py data22_13p6TeV.00427929.physics_Main.recon.HIST.f1250._lb0100._0003.1 PixelModuleFeMask-Run3-MinBias-01 --conditionsTag PixelModuleFeMask-RUN2-DATA-UPD4-09
```

To inspect the custom dead module map created above, one can do the following:
```bash
plot_deadmodulemap.py --dqm data22_13p6TeV.00427929.physics_Main.recon.HIST.f1250._lb0100._0003.1 --conditionsTag PixelModuleFeMask-Run3-MinBias-01 --db 'sqlite://schema=mycool.db;dbname=OFLP200'
```

## Tips
All official DQM histograms can be found here:
```
/eos/atlas/atlastier0/tzero/prod/data22_13p6TeV
```

## Testing Custom Maps
The custom dead module map can be tested in simulated events by including the
following `postExec` as part of the digitization step:
```
'all:conddb.blockFolder("/PIXEL/PixelModuleFeMask"); conddb.addFolder("", "<db>sqlite://;schema=/path/to/mycool.db;dbname=OFLP200</db> /PIXEL/PixelModuleFeMask", className="CondAttrListCollection", force=True); conddb.addOverride("/PIXEL/PixelModuleFeMask","PixelModuleFeMask-Run3-MinBias-01")'
```

A DQM histogram can be created in the RDO→ESD stage with the `--outputHISTFile`,
along with the usual options necessary for reconstruction. The HIST output can
then then be fed into the `plot_deadmodulemap.py` tool.